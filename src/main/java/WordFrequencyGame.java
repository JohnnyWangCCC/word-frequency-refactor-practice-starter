import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String REGEXE_STRING = "\\s+";

    public String getResult(String inputStr) {
        String[] inputStrSplitItems = inputStr.split(REGEXE_STRING);
        try {
            List<WordFrequency> wordFrequencyList = getWordFrequencyList(inputStrSplitItems);
            Map<String, List<WordFrequency>> inputMap = groupInputByWord(wordFrequencyList);
            wordFrequencyList = buildInputListWithWordCount(inputMap);
            sortWordFrequencyList(wordFrequencyList);
            return formatResult(wordFrequencyList);
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private static void sortWordFrequencyList(List<WordFrequency> wordFrequencyList) {
        wordFrequencyList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
    }

    private static String formatResult(List<WordFrequency> wordFrequencyList) {
        StringJoiner resultStringJoiner = new StringJoiner("\n");
        wordFrequencyList.forEach(wordFrequency -> resultStringJoiner.add(wordFrequency.getFullInputString()));
        return resultStringJoiner.toString();
    }

    private static List<WordFrequency> buildInputListWithWordCount(Map<String, List<WordFrequency>> inputMap) {
        return inputMap.entrySet()
                .stream()
                .map(inputMapEntry -> new WordFrequency(inputMapEntry.getKey(), inputMapEntry.getValue().size()))
                .collect(Collectors.toList());
    }

    private static List<WordFrequency> getWordFrequencyList(String[] inputStrSplitItems) {
        return Arrays.stream(inputStrSplitItems)
                .map(inputStrSplitItem ->  new WordFrequency(inputStrSplitItem, 1))
                .collect(Collectors.toList());
    }


    private Map<String, List<WordFrequency>> groupInputByWord(List<WordFrequency> wordFrequencyList) {
        Map<String, List<WordFrequency>> inputStringMap = new HashMap<>();
        wordFrequencyList.forEach(wordFrequency -> {
            List<WordFrequency> wordFrequencyListItem = inputStringMap.getOrDefault(wordFrequency.getWord(), new ArrayList<WordFrequency>());
            wordFrequencyListItem.add(wordFrequency);
            inputStringMap.put(wordFrequency.getWord(), wordFrequencyListItem);
        });
        return inputStringMap;
    }


}
