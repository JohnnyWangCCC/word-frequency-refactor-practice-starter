### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	Today the class was given a code review in the morning, along with the first group showcase. In the afternoon, we learned what code smell is and practiced how to refactor code smell. The first group cooperation was very smooth, everyone's performance and cooperation were very good; The code review also gradually found that the quality of our code has been greatly improved; In the afternoon, we showed how to recognize code smell in detail. We also recognized some problems of our own code in the past. We also learned how to refactor code step by step through practice: extract constant variable,extract method,format code,delete useless code,use stream api. and so on.

### R (Reflective): Please use one word to express your feelings about today's class.

​	Cheerful.

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	Today's lesson is really instructive at work, improving coding skills and reminding others on the team to eliminate code smell.

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	Through the understanding of code smell and the practice of refactor, I can truly feel the problems in my coding work in the past. I will carry out reasonable reconfiguration of the business in accordance with the process in the future work.
